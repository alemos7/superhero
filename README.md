# SuperHero

Hero microservice

Below is the documentation of the Hero microservice api, it has six (endpoint) that allow to have an administration on the Marvel Super hero characters.

In this Api you can Consult all, Consult one in particular, Create, Modify, Search by name and Delete. It should be noted that in order to carry out any query that is not a reading query, it is necessary to have administrator access, for which said user is provided and will be key in this documentation.


## API Documentation

https://documenter.getpostman.com/view/3612479/UVeMK4PW#02efc134-dc1d-4280-8cb1-8ae06eafeceb

## Collection Postman

https://gitlab.com/alemos7/superhero/-/blob/main/Hero.postman_collection.json

## DockerFile

https://gitlab.com/alemos7/superhero/-/blob/main/Dockerfile

## Credentials

### User
user: admin <br />
password: 123456

### Client
Basic Auth   <br />
user: client <br />
password: 123456 

## Endpoint

Public  (Basic Auth) POST oauth/token <br />
Public  GET  getAll <br />
Public  GET  getById <br />
Public  GET  getByNameLike <br />
PRIVATE (Authorization Bearer Token) POST insert <br />
PRIVATE (Authorization Bearer Token) PUT  update <br />
PRIVATE (Authorization Bearer Token) DEL  Delete <br />
