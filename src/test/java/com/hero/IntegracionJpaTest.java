package com.hero;

import com.hero.models.Character;
import com.hero.repositories.CharacterRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import static org.junit.jupiter.api.Assertions.*;
import java.util.NoSuchElementException;
import java.util.Optional;

@DataJpaTest
public class IntegracionJpaTest {

    @Autowired
    CharacterRepository characterRepository;

    @Test
    void testCharacterFindById() {
        var character =  characterRepository.findById(3L);
        assertTrue(character.isPresent());
        assertEquals("Spider Man", character.orElseThrow().getName());
    }

    @Test
    void testCharacterfindByNameContainingIgnoreCase() {
        var characters =  characterRepository.findByNameContainingIgnoreCase("man");

        assertEquals(4, characters.size());

        assertEquals("Iroman", characters.get(0).getName());
        assertEquals("Superman", characters.get(1).getName());
        assertEquals("Spider Man", characters.get(2).getName());
        assertEquals("Manolito el fuerte", characters.get(3).getName());
    }

    @Test
    void testCharacterFindAll() {
        var characters = characterRepository.findAll();
        assertFalse(characters.isEmpty());
        assertEquals(7, characters.size());
    }

    @Test
    void testSave() {
        // Given
        var character =  new Character(null, "Armando");
        // When
        var newCharacter = characterRepository.save(character);
        // Then
        assertEquals("Armando", newCharacter.getName());
    }

    @Test
    void testUpdate() {
        // Given
        var character =  new Character(null, "Pedro");
        // When
        var saveCharacter = characterRepository.save(character);
        // Then
        assertEquals("Pedro", saveCharacter.getName());
        // When
        saveCharacter.setName("PedroMan");
        var updateCharactera = characterRepository.save(saveCharacter);
        // Then
        assertEquals("PedroMan", updateCharactera.getName());
    }

    @Test
    void testDelete() {
        var character = characterRepository.findById(1L).orElseThrow();
        assertEquals("Iroman", character.getName());

        characterRepository.delete(character);

        assertThrows(NoSuchElementException.class, () -> {
            characterRepository.findById(1L).orElseThrow();
        });
        assertEquals(6, characterRepository.findAll().size());
    }

    @Test
    void testDeleteById() {

        characterRepository.deleteById(1L);

        assertThrows(NoSuchElementException.class, () -> {
            characterRepository.findById(1L).orElseThrow();
        });
        assertEquals(6, characterRepository.findAll().size());
    }

    @Test
    void testExistsByName() {
        assertTrue(characterRepository.existsByName("Iroman"));
    }

}
