package com.hero.controllers;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static  com.hero.data.Data.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hero.models.Character;
import com.hero.services.CharacterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CharacterControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private CharacterService characterService;

    @MockBean
    UserDetailsService userDetailsService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testDetail() throws Exception {
        //Given
        when(characterService.finById(1L)).thenReturn(crearCharacter001().orElseThrow());
        when(characterService.existsById(1L)).thenReturn(true);
        //When
        mvc.perform(get("/api/v1/heros/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("SpiderMan"));
    }

    @Test
    void testSave() throws Exception {
        // Given
        Character character = new Character(null, "Armando Man");
        when(characterService.save(any())).then(invocation ->{
            Character c = invocation.getArgument(0);
            c.setId(8L);
            return c;
        });

        // when
        mvc.perform(post("/api/v1/heros").contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(character)))
            // Then
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(8))
            .andExpect(jsonPath("$.name").value("Armando Man"));

        verify(characterService).save(any());

    }

}