package com.hero;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.hero.data.Data;
import com.hero.models.Character;
import com.hero.repositories.CharacterRepository;
import com.hero.services.CharacterService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


@SpringBootTest
class CharacterApplicationTests {

	@MockBean
	CharacterRepository characterRepository;

	@Autowired
	CharacterService characterService;

	@Test
	void testFindById() {
		when(characterRepository.findById(1L)).thenReturn(Data.crearCharacter001());
		Character character = characterRepository.findById(1L).orElseThrow();
		assertEquals("SpiderMan", character.getName());
		verify(characterRepository, times(1)).findById(1L);
	}



}
