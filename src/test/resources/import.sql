INSERT INTO characters (name) VALUES ('Iroman');
INSERT INTO characters (name) VALUES ('Superman');
INSERT INTO characters (name) VALUES ('Spider Man');
INSERT INTO characters (name) VALUES ('Thor');
INSERT INTO characters (name) VALUES ('Storm');
INSERT INTO characters (name) VALUES ('Venon');
INSERT INTO characters (name) VALUES ('Manolito el fuerte');

INSERT INTO users (username, password, enabled) VALUES ('armando','$2a$10$NjAVjYtANBkSbWJoLPhBJeak49BZSHUqXHAXc2naY1X7R15Z3nd/S',1);
INSERT INTO users (username, password, enabled) VALUES ('admin','$2a$10$1cWbflAEq29EjsNvT7sbJ.7jaYyWEbJyOmgrhNSEblKnFy/1VuTAC',1);

INSERT INTO profiles (name) VALUES ('ROLE_USER');
INSERT INTO profiles (name) VALUES ('ROLE_ADMIN');

INSERT INTO users_profiles (user_id, profiles_id) VALUES (1,1);
INSERT INTO users_profiles (user_id, profiles_id) VALUES (2,2);