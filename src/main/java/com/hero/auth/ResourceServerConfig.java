package com.hero.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(HttpMethod.GET,"/api/v1/heros").permitAll()
            .antMatchers(HttpMethod.GET,"/api/v1/heros/{id}").permitAll()
            .antMatchers(HttpMethod.GET,"/api/v1/heros/name/{name}").permitAll()
            .antMatchers(HttpMethod.POST,"/api/v1/heros").permitAll()
            .anyRequest().authenticated();
    }


}
