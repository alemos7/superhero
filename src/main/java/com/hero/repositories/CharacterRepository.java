package com.hero.repositories;

import com.hero.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CharacterRepository extends JpaRepository<Character, Long> {
    public List<Character> findByNameContainingIgnoreCase(String name);
    public Boolean existsByName(String name);
}
