package com.hero.services;

import com.hero.models.Character;
import com.hero.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterServiceImpl implements CharacterService {

    private CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public List<Character> finAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character finById(Long id) {
        //return characterRepository.finById(id);
        return characterRepository.findById(id).orElseThrow();
    }

    @Override
    public Character save(Character character) {
        return characterRepository.save(character);
    }

    @Override
    public void delete(Character character) {
        characterRepository.delete(character);
    }

    @Override
    public void deleteById(Long id) {
        characterRepository.deleteById(id);
    }

    @Override
    public List<Character> findByNameContainingIgnoreCase(String name) {
        return characterRepository.findByNameContainingIgnoreCase(name);
    }

    @Override
    public Boolean existsByName(String name) {
        return characterRepository.existsByName(name);
    }

    @Override
    public Boolean existsById(Long id) {
        return characterRepository.existsById(id);
    }
}
