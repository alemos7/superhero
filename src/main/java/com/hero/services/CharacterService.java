package com.hero.services;

import com.hero.models.Character;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CharacterService {
    List<Character> finAll();
    Character finById(Long id);
    Character save(Character character);
    void delete(Character character);
    void deleteById(Long id);
    List<Character> findByNameContainingIgnoreCase(String name);
    Boolean existsByName(String name);
    Boolean existsById(Long id);
}
