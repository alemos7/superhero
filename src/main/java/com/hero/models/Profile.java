package com.hero.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="profiles")
public class Profile  implements Serializable {

    private static final long serialVersionUID = 1805345006159653872L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, length = 20)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
