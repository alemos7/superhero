package com.hero.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class CharacterException extends RuntimeException{

    private String code;
    private HttpStatus status;

    public CharacterException(String code, HttpStatus status, String message) {
        super(message);
        this.code = code;
        this.status = status;
    }
}
