package com.hero.controllers;

import com.hero.exceptions.CharacterException;
import com.hero.exceptions.dto.ErrorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value = CharacterException.class)
    public ResponseEntity<ErrorDTO> requestExceptionHandler(CharacterException exception){
        ErrorDTO error = ErrorDTO.builder().code(exception.getCode()).message(exception.getMessage()).build();
        return new ResponseEntity<>(error, exception.getStatus());
    }
}
