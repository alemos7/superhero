package com.hero.controllers;

import static org.springframework.http.HttpStatus.*;

import com.hero.annotations.ResponseTime;
import com.hero.exceptions.CharacterException;
import com.hero.exceptions.ErrorCatalogue;
import com.hero.models.Character;
import com.hero.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@EnableCaching
@RestController
@RequestMapping("/api/v1/heros")
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @ResponseTime
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    @Cacheable(value = "character")
    public Character detail(@PathVariable Long id) {
        return characterService.finById(id);
    }

    @ResponseTime
    @GetMapping("")
    @ResponseStatus(OK)
    @Cacheable(value = "character")
    public List<Character> list() {
        return characterService.finAll();
    }

    @ResponseTime
    @GetMapping("/name/{name}")
    @ResponseStatus(OK)
    @Cacheable(value = "character")
    public List<Character> findByNameContainingIgnoreCase(@PathVariable String name) {
        return characterService.findByNameContainingIgnoreCase(name);
    }

    @ResponseTime
    @PostMapping
    @ResponseStatus(CREATED)
    @CacheEvict(value = "character", allEntries = true)
    public Character save(@RequestBody Character character) {
        if(character.getName() == null || character.getName() == "")
            throw new CharacterException(
                    ErrorCatalogue.BAD_REQUEST,
                    BAD_REQUEST,
                    ErrorCatalogue.BAD_REQUEST_DESC);

        if(characterService.existsByName(character.getName()))
            throw new CharacterException(
                    ErrorCatalogue.REGISTRATION_EXISTS,
                    INTERNAL_SERVER_ERROR,
                    ErrorCatalogue.REGISTRATION_EXISTS_DESC);

        return characterService.save(character);
    }

    @ResponseTime
    @PutMapping("/{id}")
    @ResponseStatus(OK)
    @CacheEvict(value = "character", allEntries = true)
    public Character update(@PathVariable Long id, @RequestBody Character character) {
        if(!characterService.existsById(id))
            throw new CharacterException(
                    ErrorCatalogue.DOES_NOT_EXIST,
                    NOT_FOUND,
                    ErrorCatalogue.DOES_NOT_EXIST_DESC);

        Character updateCharacter = characterService.finById(id);
        updateCharacter.setName(character.getName());
        return characterService.save(updateCharacter);
    }

    @ResponseTime
    @DeleteMapping("/{id}")
    @ResponseStatus(ACCEPTED)
    @CacheEvict(value = "character", allEntries = true)
    public void delete(@PathVariable Long id) {
        if(!characterService.existsById(id))
            throw new CharacterException(
                    ErrorCatalogue.DOES_NOT_EXIST,
                    NOT_FOUND,
                    ErrorCatalogue.DOES_NOT_EXIST_DESC);
        characterService.deleteById(id);
    }



}
