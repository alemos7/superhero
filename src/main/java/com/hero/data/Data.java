package com.hero.data;

import com.hero.models.Character;
import java.util.Optional;

public class Data {

    public static Optional<Character> crearCharacter001() {
        return Optional.of(new Character(1L, "SpiderMan"));
    }
    public static Optional<Character> crearCharacter002() {
        return Optional.of(new Character(2L, "IronMan"));
    }
    public static Optional<Character> crearCharacter003() {
        return Optional.of(new Character(3L, "Hulk"));
    }
    public static Optional<Character> crearCharacter004() {
        return Optional.of(new Character(4L, "Superman"));
    }
    public static Optional<Character> crearCharacter005() {
        return Optional.of(new Character(4L, "Manolito el fuerte"));
    }
    public static Optional<Character> crearCharacter006() {
        return Optional.of(new Character(4L, "Thor"));
    }

}
